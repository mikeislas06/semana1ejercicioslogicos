const reverseInt = (number) => {
  if (typeof number === 'number') {
    return (
      parseFloat(number.toString().split('').reverse().join('')) *
      Math.sign(number)
    );
  }
  throw new Error('Error: Tipo de dato no admitido');
};

module.exports = {
  reverseInt,
};

const reverseString = (str) => {
  //Implementación
  if (typeof str === 'string' && str.length > 1 && str.length < 15) {
    return str.split('').reverse().join('');
  } else {
    throw new Error('Error: Tipo de dato o longitud no admitidos');
  }
};

module.exports = {
  reverseString,
};
